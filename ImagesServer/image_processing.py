from colorthief import ColorThief
import webcolors
import cv2

IMAGE_PATH = "images-files/sample-image.png"
CASCADE_PATH = "cascades/haarcascade_frontalface_alt.xml"
DETECTED_IMAGE_PATH = "images-files/detected-image.jpg"


def get_faces_in_image():
    # Load the cascade
    face_cascade = cv2.CascadeClassifier(CASCADE_PATH)
    # RGB to gray scale
    image = cv2.imread(IMAGE_PATH)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # get detected faces
    faces = face_cascade.detectMultiScale(gray_image)
    # get number of detected faces
    faces_in_image = len(faces)

    # create image with detected faces
    for (x, y, w, h) in faces:
        cv2.rectangle(image, (x, y), (x + w, y + h), (212, 11, 68), 2)
    # save image with detected faces locally
    cv2.imwrite(DETECTED_IMAGE_PATH, image)

    # return number of detected faces
    return faces_in_image


def get_closest_colour_name(requested_colour):
    min_colours = {}
    for key, name in webcolors.CSS3_HEX_TO_NAMES.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]


def get_colour_name(req_colour):
    try:
        act_name = webcolors.rgb_to_name(req_colour)
        clo_name = act_name
    except ValueError:
        clo_name = get_closest_colour_name(req_colour)
        act_name = None
    return act_name, clo_name


def get_dominant_color():
    # get dominant color RGB
    color_thief = ColorThief(IMAGE_PATH)
    dominant_color_rgb = color_thief.get_color(quality=2)

    # get dominant color name
    actual_name_color, closest_name_color = get_colour_name(dominant_color_rgb)

    # return dominant color RGB & name
    return dominant_color_rgb, closest_name_color