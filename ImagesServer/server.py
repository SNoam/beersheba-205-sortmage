from flask import Flask, request, jsonify
from flask_cors import CORS
import requests
import image_processing

IMAGE_PATH = "images-files/sample-image.png"
HOST = '127.0.0.1'
PORT = 4000


app = Flask(__name__)
CORS(app)


@app.route('/api/photoAnalyzer', methods=['POST'])
def photo_analyzer():
    # get image url
    request_data = request.get_json()
    url = request_data["url"]

    # get image
    image = requests.get(url)

    # save image locally
    with open(IMAGE_PATH, "wb") as image_file:
        image_file.write(image.content)

    # do image processing
    dominant_color_rgb, dominant_color_name = image_processing.get_dominant_color()

    faces_in_image = image_processing.get_faces_in_image()

    # return image processing results
    response = jsonify({'attributes': {'dominantColor': {'RGB': dominant_color_rgb, 'name': dominant_color_name},
                                       'facesInImage': faces_in_image}})

    return response


if __name__ == '__main__':
    app.run(host=HOST, port=PORT, threaded=True)
