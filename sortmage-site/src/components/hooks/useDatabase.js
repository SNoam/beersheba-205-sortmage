import { useState, useEffect } from "react";
import { firestore } from "../../firebase";
import { useAuth } from "../../contexts/AuthContext";

const useDatabase = (collection = "images") => {
  const [docs, setDocs] = useState([]);
  const { currentUser } = useAuth();
  useEffect(() => {
    const unsub = firestore
      .collection(collection)
      .where("userID", "==", currentUser.uid)
      .orderBy("createdAt", "desc")
      .onSnapshot((snap) => {
        let documents = [];
        snap.forEach((doc) => {
          documents.push({ ...doc.data(), id: doc.id });
        });
        setDocs(documents);
      });

    return () => unsub();
    // this is a cleanup function that react will run when
    // a component using the hook unmounts
  }, [collection, currentUser]);

  return { docs };
};

export default useDatabase;
