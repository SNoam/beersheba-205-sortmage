import React from "react";
import Signup from "./authentication/Signup";
import Login from "./authentication/Login";
import UserProfile from "./authentication/UserProfile";
import UpdateProfile from "./authentication/UpdateProfile";
import UploadImage from "./functionality/UploadImage";
import Gallery from "./functionality/Gallery";
import ForgotPassword from "./authentication/ForgotPassword";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PrivateRoute from "./functionality/PrivateRoute";
import Navbar from "./functionality/Navbar";

function App() {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <PrivateRoute exact path="/" />
          <PrivateRoute path="/user" component={UserProfile} />
          <Route path="/signup" component={Signup} />
          <Route path="/login" component={Login} />
          <Route path="/forgot-password" component={ForgotPassword} />
          <PrivateRoute path="/update-profile" component={UpdateProfile} />
          <PrivateRoute path="/upload-image" component={UploadImage} />
          <PrivateRoute path="/gallery" component={Gallery} />
        </Switch>
      </Router>
    </>
  );
}

export default App;
