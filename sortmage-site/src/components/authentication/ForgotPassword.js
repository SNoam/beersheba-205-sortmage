import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../../contexts/AuthContext";
import { Link } from "react-router-dom";
import CenteredContainer from "../functionality/CenteredContainer";

export default function ForgotPassword() {
  const emailRef = useRef();
  const { resetPassword } = useAuth();
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      setError("");
      setMessage("");
      setLoading(true);
      await resetPassword(emailRef.current.value);
      setMessage("Check your inbox for further instructions!");
    } catch {
      setError("Failed to reset password!");
    }
    setLoading(false);
  }

  return (
    <CenteredContainer>
      <Card className="shadow">
        <Card.Body>
          <h1 className="text-center mb-4 mt-2 ">Password Reset</h1>
          {error && <Alert variant="danger">{error}</Alert>}
          {message && <Alert variant="success">{message}</Alert>}
          <Form onSubmit={handleSubmit}>
            <Form.Group id="email">
              <Form.Label>Email:</Form.Label>
              <Form.Control type="email" ref={emailRef} required />
            </Form.Group>
            <Button disabled={loading} className="mt-4" type="submit" block>
              Reset password
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <Card className="shadow w-100 mt-2">
        <Card.Body className="text-center">
          <Link to="/login">
            <p className="h6 ">Log In</p>
          </Link>
        </Card.Body>
      </Card>
    </CenteredContainer>
  );
}
