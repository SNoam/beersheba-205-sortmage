import React, { useState } from "react";
import { Card, Button, Alert } from "react-bootstrap";
import { useAuth } from "../../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";
import CenteredContainer from "../functionality/CenteredContainer";

export default function UserProfile() {
  const [error, setError] = useState("");
  const { currentUser, logout } = useAuth();
  const history = useHistory();

  async function handleLogout() {
    setError("");

    try {
      await logout();
      history.push("/login");
    } catch {
      setError("Failed to logout!");
    }
  }

  return (
    <CenteredContainer>
      <Card className="shadow">
        <Card.Body>
          <h1 className="text-center mb-4 mt-2 ">Profile</h1>
          <Card.Text>
            {error && <Alert variant="danger">{error}</Alert>}
            <strong className="mx-1">Email:</strong> {currentUser.email}
          </Card.Text>
          <Link to="/update-profile" className="btn btn-primary w-100 mt-3">
            Update Profile
          </Link>
        </Card.Body>
      </Card>
      <Card className="shadow w-100 mt-2">
        <Card.Body className="text-center">
          <Button variant="link" onClick={handleLogout}>
            <p className="h6 ">Log Out</p>
          </Button>
        </Card.Body>
      </Card>
    </CenteredContainer>
  );
}
