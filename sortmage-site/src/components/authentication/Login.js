import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";
import CenteredContainer from "../functionality/CenteredContainer";

export default function Login() {
  const emailRef = useRef();
  const passwordRef = useRef();
  const { login } = useAuth();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  async function handleSubmit(event) {
    event.preventDefault();
    try {
      setError("");
      setLoading(true);
      await login(emailRef.current.value, passwordRef.current.value);
      history.push("/user");
    } catch {
      setError("Failed to log in!");
    }
    setLoading(false);
  }

  return (
    <CenteredContainer>
      <Card className="shadow">
        <Card.Body>
          <h1 className="text-center mb-4 mt-2 ">Log In</h1>
          {error && <Alert variant="danger">{error}</Alert>}
          <Form onSubmit={handleSubmit}>
            <Form.Group id="email">
              <Form.Label>Email:</Form.Label>
              <Form.Control type="email" ref={emailRef} required />
            </Form.Group>
            <Form.Group id="password">
              <Form.Label>Password:</Form.Label>
              <Form.Control type="password" ref={passwordRef} required />
            </Form.Group>
            <Button disabled={loading} className="mt-4" type="submit" block>
              Log In
            </Button>
          </Form>
          <div className="w-100 mt-3 text-center">
            <Link to="/forgot-password">Forgot Password?</Link>
          </div>
        </Card.Body>
      </Card>
      <Card className="shadow w-100 mt-2">
        <Card.Body className="text-center">
          Don't have an account? <Link to="/signup"> Sign up!</Link>
        </Card.Body>
      </Card>
    </CenteredContainer>
  );
}
