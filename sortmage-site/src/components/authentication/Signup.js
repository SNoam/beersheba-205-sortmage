import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";
import CenteredContainer from "../functionality/CenteredContainer";

export default function Signup() {
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const { signup } = useAuth();
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  async function handleSubmit(event) {
    event.preventDefault();

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Passwords do not match!");
    }

    try {
      setError("");
      setLoading(true);
      await signup(emailRef.current.value, passwordRef.current.value);
      history.push("/user");
    } catch {
      setError("Failed to create an account!");
    }
    setLoading(false);
  }

  return (
    <CenteredContainer>
      <Card className="shadow">
        <Card.Body>
          <h1 className="text-center mb-4 mt-2 ">Sign Up</h1>
          {error && <Alert variant="danger">{error}</Alert>}
          <Form onSubmit={handleSubmit} className="form-floating">
            <Form.Group id="email">
              <Form.Label>Email:</Form.Label>
              <Form.Control
                type="email"
                ref={emailRef}
                placeholder="e.g: email@example.com"
                required
              />
            </Form.Group>
            <Form.Group id="password">
              <Form.Label>Password:</Form.Label>
              <Form.Control
                type="password"
                ref={passwordRef}
                placeholder="Must contain at least 6 characters"
                required
              />
            </Form.Group>
            <Form.Group id="password-confirm">
              <Form.Label>Confirm Password:</Form.Label>
              <Form.Control
                type="password"
                ref={passwordConfirmRef}
                placeholder="Must contain at least 6 characters"
                required
              />
            </Form.Group>
            <Button disabled={loading} className="mt-4" type="submit" block>
              Sign Up
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <Card className="shadow w-100 mt-2">
        <Card.Body className="text-center">
          Already have an account? <Link to="/login"> Log In!</Link>
        </Card.Body>
      </Card>
    </CenteredContainer>
  );
}
