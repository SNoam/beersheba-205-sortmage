import React, { useState } from "react";
import useDatabase from "../hooks/useDatabase";
import { Container, Row } from "react-bootstrap";
//import ImageGallery from "react-image-gallery";
import GalleryItem from "./GalleryItem";
import ImageModal from "./ImageModal";
import "react-image-gallery/styles/css/image-gallery.css";
export default function Gallery() {
  const { docs } = useDatabase();
  const [selectedImage, setSelectedImage] = useState(null);

  return (
    <>
      <h1 className="text-center my-4">Gallery</h1>
      <Container fluid className="p-5">
        <Row>
          {docs.map((imageData) => {
            return (
              <GalleryItem
                imageSrc={imageData.url}
                key={imageData.id}
                onClick={() => setSelectedImage({ ...imageData })}
              />
            );
          })}
        </Row>
        {selectedImage && (
          <ImageModal
            selectedImage={selectedImage}
            setSelectedImage={setSelectedImage}
          />
        )}
      </Container>
    </>
  );
}
