import React from "react";
import { Navbar as Bar, Nav, Image, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
export default function Navbar() {
  return (
    <>
      <Bar collapseOnSelect bg="dark" variant="dark" expand="sm">
        <Container fluid>
          <Bar.Toggle aria-controls="responsive-navbar-nav" />
          <Link to="/user">
            <Bar.Brand>
              <Image
                alt=""
                src="https://firebasestorage.googleapis.com/v0/b/sortmage.appspot.com/o/site-res%2Ff9eb8c7ba60a4779b6f23903a0650904.png?alt=media&token=64f0a71e-6e2e-4691-a83b-93db91f31082"
                width="40"
                height="40"
                className="d-inline-block align-center"
                rounded
              />{" "}
              Sortmage
            </Bar.Brand>
          </Link>
          <Bar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link as={Link} to="/upload-image">
                Upload photo
              </Nav.Link>
              <Nav.Link as={Link} to="/gallery">
                Gallery
              </Nav.Link>
              <Nav.Link as={Link} to="/user">
                Profile
              </Nav.Link>
            </Nav>
          </Bar.Collapse>
        </Container>
      </Bar>
    </>
  );
}
