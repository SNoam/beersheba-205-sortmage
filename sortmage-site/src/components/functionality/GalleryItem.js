import React from "react";
import { Col, Image } from "react-bootstrap";
export default function GalleryItem({ imageSrc, onClick }) {
  return (
    <Col xl={3} lg={4} md={6} className="mb-4">
      <Image
        src={imageSrc}
        rounded
        fluid
        className="w-100 h-100 shadow"
        onClick={onClick}
      />
    </Col>
  );
}
