import React, { useState } from "react";
import axios from "axios";
import { Form, Button, Card, Alert, ProgressBar, Image } from "react-bootstrap";
import { useAuth } from "../../contexts/AuthContext";
import { storage, firestore, getTimeStamp } from "../../firebase";
import { Link } from "react-router-dom";
import CenteredContainer from "./CenteredContainer";

export default function UploadImage() {
  const [image, setImage] = useState(null);
  const [url, setURL] = useState("");
  const [progress, setProgress] = useState(0);
  const { currentUser } = useAuth();
  const [error, setError] = useState("");
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);

  function handleChange(event) {
    setImage(event.target.files[0]);
  }

  async function handleUpload(event) {
    event.preventDefault();

    setError("");
    setMessage("");
    setURL("");

    if (!image) {
      setError("An Image was not chosen!");
      return;
    }

    if (!image.type.startsWith("image")) {
      setError("Chosen file wan not an image!");
      return;
    }
    setLoading(true);

    const userID = currentUser.uid;
    const imagePath = `/${userID}/images/${image.name}`;
    const storageRef = storage.ref(imagePath);
    const collectionRef = firestore.collection("images");

    const uploadTask = storageRef.put(image);
    uploadTask.on(
      "state_changed",
      (snapshot) => {
        setProgress(
          Math.round((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
        );
      },
      (error) => {
        setError("Failed to upload image!");
      },
      async () => {
        let url = "";
        let message = "";
        try {
          const createdAt = getTimeStamp();
          url = await storageRef.getDownloadURL();

          const imageAttributes = await axios.post(
            process.env.REACT_APP_SERVER_IP,
            { url }
          );
          console.log(imageAttributes.data);
          await collectionRef.add({
            userID,
            url,
            createdAt,
            name: image.name,
            attributes: imageAttributes.data.attributes,
          });

          message = "Image was uploaded! successfully";
        } catch (e) {
          setError("Failed to proccess or upload the image!");
        } finally {
          setImage(null);
          setProgress(0);
          setMessage(message);
          setURL(url);
          setLoading(false);
        }
      }
    );
  }

  return (
    <CenteredContainer>
      <Card>
        <Card.Body>
          <h1 className="text-center mb-4 mt-2 ">Image Upload</h1>
          {error && <Alert variant="danger">{error}</Alert>}
          {message && <Alert variant="success">{message}</Alert>}
          {progress > 0 && (
            <ProgressBar
              className="my-1"
              animated={loading}
              now={progress}
              label={`${progress}%`}
            />
          )}
          <Form onSubmit={handleUpload}>
            <Form.Group id="file">
              <Form.File custom>
                <Form.File.Input
                  onChange={handleChange}
                  accept="image/png, image/jpeg, image/jpg"
                />
                <Form.File.Label>
                  {image ? image.name : "Select Image to Upload"}
                </Form.File.Label>
              </Form.File>
            </Form.Group>
            <Button
              disabled={loading || image == null}
              className="mt-4"
              type="submit"
              block
            >
              Upload
            </Button>
          </Form>
        </Card.Body>
      </Card>
      <Image className="w-100 my-2" src={`${url}`} thumbnail />
      <Card className="w-100 mt-2">
        <Card.Body className="text-center">
          <Link to="/user">
            <p className="h6">Go Back</p>
          </Link>
        </Card.Body>
      </Card>
    </CenteredContainer>
  );
}
