import React from "react";
import { Modal, Button, Image, Badge } from "react-bootstrap";
export default function ImageModal({ selectedImage, setSelectedImage }) {
  const handleClose = () => setSelectedImage(null);
  return (
    <Modal
      show={selectedImage}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
      size="md"
    >
      <Modal.Header closeButton>
        <Modal.Title>{selectedImage.name}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Image
          src={selectedImage.url}
          rounded
          fluid
          className="shadow h-100 w-100"
        />
      </Modal.Body>
      <Modal.Footer className="d-flex justify-content-center align-items-center">
        <Button variant="danger" block className="w-25">
          Faces{" "}
          <Badge variant="light">{selectedImage.attributes.facesInImage}</Badge>
        </Button>
        <Button
          variant="light"
          style={{
            color: "white",
            backgroundColor: selectedImage.attributes.dominantColor.name,
          }}
        >
          Dominant Color Name{" "}
          <Badge variant="light">
            {selectedImage.attributes.dominantColor.name}
          </Badge>
        </Button>
        <Button
          variant="light"
          block
          className="w-75"
          style={{
            color: "white",
            backgroundColor: `rgb(${selectedImage.attributes.dominantColor.RGB[0]}, ${selectedImage.attributes.dominantColor.RGB[1]}, ${selectedImage.attributes.dominantColor.RGB[2]})`,
          }}
        >
          Dominant Color RGB{" "}
          <Badge variant="light">
            {selectedImage.attributes.dominantColor.RGB[0] +
              ", " +
              selectedImage.attributes.dominantColor.RGB[1] +
              ", " +
              selectedImage.attributes.dominantColor.RGB[2]}
          </Badge>
        </Button>

        <Button
          variant="secondary"
          onClick={handleClose}
          block
          className="w-25"
        >
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
}
